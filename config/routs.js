const products = require('../app/controllers/products');
const auth = require('../app/controllers/auth');
const authMiddleware = require('../app/middleware/auth');
const car = require('../app/controllers/cars');

module.exports = (app) => {
    //test_product
    app.get('/products', products.getAll);
    app.post('/productscreate', products.create);
    app.post('/productsput', products.update);
    app.post('/productsremove', products.remove);

    //auth
    app.post('/login', auth.signIn);
    app.post('/logout', authMiddleware, auth.logout);
    app.post('/signup', auth.register);

    //user
    app.get('/profile', authMiddleware, auth.getUserInfo);
    app.get('/token', authMiddleware, auth.getUserToken);
    app.get('/avatar', authMiddleware, auth.getUserInfoAvatar);
    app.get('/allprofileinfo', authMiddleware, auth.getUserAllInfo);
    app.post('/avatarupdate', authMiddleware, auth.updateAvatar);
    app.post('/emailupdate', authMiddleware, auth.updateEmail);
    app.post('/fioupdate', authMiddleware, auth.updateFio);
    app.post('/passwordupdate', authMiddleware, auth.updatePassword);

    //car
    app.get('/cars', car.cars);
    app.post('/carinfo',authMiddleware, car.carinfo);
    app.get('/books',authMiddleware, car.books);
    app.get('/favorites',authMiddleware, car.favorites);
    app.post('/newbook',authMiddleware, car.newbook);
    app.post('/newfavorites',authMiddleware, car.newfavorites);
    app.post('/removebook', car.removebook);
    app.post('/removefavorites', car.removefavorites);
    app.post('/removeallbook',authMiddleware, car.removeallbook);
    app.post('/removeallfavorites',authMiddleware, car.removeallfavorites);
};