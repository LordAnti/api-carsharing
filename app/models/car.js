const mongoose = require('mongoose');
const ObjectId = require('mongodb').ObjectID;

const CarSchema = new mongoose.Schema({
    _id:{
        type: ObjectId,
        required: true
    },
    longitude: {
        type: String,
        required: true
    },
    latitude: {
        type: String,
        required: true
    },
    mark:{
        type: String,
        required: true
    }
});

mongoose.model('Car',CarSchema);