const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    id: String,
    name: String,
    price: String,
});

mongoose.model('Product', ProductSchema);