const mongoose = require('mongoose');
const bCrypt = require('bcrypt');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const authHelper = require('../helpers/authHelper');

const { secret } = require('../../config/app').jwt;
const { registerValidation, loginValidation } = require('../../validation');

const User = mongoose.model('User');
const Token = mongoose.model('Token');



/**
 * 
 * @param {*} req Запрос
 * @param {*} res Ответ
 */
const logout = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userID = decode.userId;
    Token.deleteOne({userId:userID}, req.body)
    .exec()
    .then(() => res.json({success: true}))
    .catch(err => res.status(500).json(err));
};

/**
 *Функция замены токенов
 *
 * @param {*} userId
 * @returns
 */
const updateTokens = (userId) => {
    const accessToken = authHelper.generateAccessToken(userId);
    const refreshToken = authHelper.generateRefreshToken();

    return authHelper.replaceDbRefreshToken(refreshToken.id, userId)
        .then(() => ({
            accessToken,
            refreshToken: refreshToken.token,
        }));
};

/**
 *Авторизация пользователя
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const signIn = (req, res) => {
    //Validate the data
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    
    const { email, password } = req.body;
    User.findOne({ email })
        .exec()
        .then((user) => {
            if (!user) {
                res.status(401).json({ message: 'User does not exist!' });
            }

            const isValid = bCrypt.compareSync(password, user.password);

            if (isValid) {
                updateTokens(user._id).then(tokens => res.json(tokens));
            } else {
                res.status(401).json({ message: 'Invalid credentials!' });
            }
        }).catch(() => res.status(500).json({ message: 'Govno' }));
};

/**
 *Регистрация пользователя
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const register = async (req, res) => {
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const emailExist = await User.findOne({ email: req.body.email });
    if (emailExist) return res.status(400).send('Email already exist!');

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    const user = new User({
        fio: req.body.fio,
        email: req.body.email,
        password: hashedPassword,
        avatar: req.body.avatar
    });
    try {
        const savedUser = await user.save();
        res.send({ user: user._id });
    } catch (e) {
        res.status(400).send(e);
    }

};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const updatePassword = async (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body, salt);

    var userId = decode.userId;

    User.findOneAndUpdate({ _id: userId }, hashedPassword)
    .exec()
    .then(user => res.json(user))
    .catch(err => res.status(500).json(err));
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const updateEmail = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;

    User.findOneAndUpdate({ _id: userId }, req.body)
    .exec()
    .then(user => res.json(user))
    .catch(err => res.status(500).json(err));
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const updateFio = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;

    User.findOneAndUpdate({ _id: userId }, req.body)
    .exec()
    .then(user => res.json(user))
    .catch(err => res.status(500).json(err));
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const updateAvatar = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;

    User.findOneAndUpdate({ _id: userId }, req.body)
    .exec()
    .then(user => res.json(user))
    .catch(err => res.status(500).json(err));
};

/**
 *Получение информации о пользователе
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const getUserInfo = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;
    User.findOne({ _id: userId }).then((user) => {
        return res.send(user.fio);
    }).catch(err => res.status(400).json({ message: err.message }));
};

/**
 *Получение информации о токене для прохождения авторизации
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const getUserToken = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userID = decode.userId;
    Token.findOne({ userId: userID }).then((token) => {
        return res.send(token.tokenId);
    }).catch(err => res.status(400).json({ message: err.message }));
};

/**
 *Получение информации о пользователе полностью
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const getUserAllInfo = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;
    User.findOne({ _id: userId }).then((user) => {
        return res.send(
            {
                fio: user.fio,
                email: user.email,
                password: user.password,
                avatar: user.avatar
        });
    }).catch(err => res.status(400).json({ message: err.message }));
};

/**
 *Получение информации о пользователе - фото
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const getUserInfoAvatar = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;
    User.findOne({ _id: userId }).then((user) => {
        return res.send({avatar:user.avatar});
    }).catch(err => res.status(400).json({ message: err.message }));
};

/**
 *Обновление токенов
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const refreshTokens = (req, res) => {
    const { refreshToken } = req.body;
    let payload;
    try {
        payload = jwt.verify(refreshToken, secret);
        if (payload.type !== 'refresh') {
            res.status(400).json({ message: 'Invalid token!' });
            return;
        }
    } catch (e) {
        if (e instanceof jwt.TokenExpiredError) {
            res.status(400).json({ message: 'Token expired!' });
            return;
        } else if (e instanceof jwt.JsonWebTokenError) {
            res.status(400).json({ message: 'Invalid token catch!' });
            return;
        }
    }

    Token.findOne({ tokenId: payload.id })
        .exec()
        .then((token) => {
            if (token === null) {
                throw new Error('Invalid token null');
            }

            return updateTokens(token.userId);
        }).then(tokens => res.json(tokens))
        .catch(err => res.status(400).json({ message: err.message }));
};

module.exports = {
    signIn,
    refreshTokens,
    register,
    getUserInfo,
    getUserInfoAvatar,
    updateAvatar,
    updatePassword,
    updateEmail,
    updateFio,
    getUserAllInfo,
    getUserToken,
    logout,
};